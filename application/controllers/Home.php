<?php defined('BASEPATH') or exit('No direct path allowed');

class Home extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();

        $this->load->model('M_model','m',true);

    }

    public function index()
    {
        $this->load->view('index');
    }
}

?>