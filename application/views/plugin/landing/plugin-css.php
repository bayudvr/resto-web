    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <link rel="icon" type="image/png" href="<?php echo base_url() ?>assets/img/logo.png">

    <style>
        html
        {
            scroll-behavior: smooth;
        }
    </style>